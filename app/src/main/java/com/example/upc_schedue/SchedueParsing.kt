
package com.example.upc_schedue

import android.content.Context
import android.util.Log
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.RecyclerView
import org.jsoup.Jsoup
import org.jsoup.nodes.Document
import org.jsoup.nodes.Element
import org.jsoup.select.Elements
import java.lang.Exception
import java.util.Calendar


class SchedueParsing(
    val dateInput: TextView,
    val calendar: Calendar,
    val itemList: RecyclerView,
    val context: Context,
) {
    fun setSchedueToday(groupID: String) {
        val secThread = Thread(Runnable {
         try {
             Log.d("GroupID", "$groupID")
             var url = "https://uraypc.ru/ru/schedule/?group=$groupID&date_edu1c=${dateInput.text}&send=Показать#current"
             Log.d("url", "$url")
             val dayOfWeek = calendar.get(Calendar.DAY_OF_WEEK)
             val items = arrayListOf<Item>()
             val doc: Document = Jsoup.connect(url).get()
             Log.d("DayOfWeek", "$dayOfWeek")
             val headers: Elements = doc.select("h2")
             var dayIndex: Int = 0
             var dayOfMonth: String = dateInput.text.toString().substringBefore('.')
             lateinit var table: Element

             for (i in headers.indices) {
                 if(headers[i].text().toString().substringBefore(' ') == dayOfMonth) {
                     table = doc.select("table").get(dayIndex - 2)
                     dayIndex = 0
                     break
                 }
                 else { table = doc.select("table").first()}
                 dayIndex++
                 Log.d("dayIndex", "$dayIndex")
             }
             val rows: Elements = table.select("tr")
             var row: Element
             lateinit var cols: Elements
             for (i in rows.indices) {
                 row = rows.get(i)
                 cols = row.select("td")
                 if (cols.size <= 0) {
                     continue
                 }
                 else {
                     var lessonId: String = ""
                     var lessonName: String  = ""
                     var lessonTime: String  = ""
                     var lessonRoom: String = ""
                     if (cols.size >= 5) {
                         lessonId =  cols[0].text()
                         lessonName = cols[2].text()
                         lessonTime = cols[1].text()
                         lessonRoom = cols[3].text()
                     }
                     else {
                         lessonName = cols[1].text()
                         lessonTime = cols[0].text()
                         lessonRoom = cols[2].text()
                     }
                     Log.d("cols size", "${cols.size}")
                     items.add(Item(lessonId, lessonName, lessonTime, lessonRoom))
                 }
             }
             (context as AppCompatActivity).runOnUiThread() {
                 itemList.adapter = ItemsAdapter(items, context)
             }
         } catch (e: Exception) {
             e.printStackTrace()
         }
        })
            secThread.start()
        }
}

