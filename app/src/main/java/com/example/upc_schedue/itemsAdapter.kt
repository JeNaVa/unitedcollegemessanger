package com.example.upc_schedue

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView


class ItemsAdapter (
    var items: List<Item>,
    var context: Context,
) : RecyclerView.Adapter<ItemsAdapter.MyViewHolder> () {

    class MyViewHolder(view: View): RecyclerView.ViewHolder(view) {
        val lessonNumber: TextView = view.findViewById(R.id.numberLesson)
        val nameLesson: TextView = view.findViewById(R.id.nameLesson)
        val limeLesson: TextView = view.findViewById(R.id.timeLesson)
        val lessonRoom: TextView = view.findViewById(R.id.lessonRoom)

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.schedue_item, parent,false)
        return MyViewHolder(view)
    }

    override fun getItemCount(): Int {
        return items.count()
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.lessonNumber.text = items[position].lessonId
        holder.nameLesson.text = items[position].name
        holder.limeLesson.text = items[position].time
        holder.lessonRoom.text = items[position].lessonRoom
    }
}