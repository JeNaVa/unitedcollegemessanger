package com.example.upc_schedue

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.TextView;
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.util.Log
import android.widget.Button
import android.widget.Toast

import java.util.Calendar

class MainActivity : AppCompatActivity() {
    var offline: Boolean = false
    var request: String = ""
    var count: Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val dateInput: TextView = findViewById(R.id.dateInput)
        val groupInput: TextView = findViewById(R.id.groupNameInput)
        val itemList = findViewById<RecyclerView>(R.id.itemList)
        val checkSchedue: Button = findViewById(R.id.schedueCheck)
        val nextSchedueDay: Button = findViewById(R.id.nextDay)
        val pastSchedueDay: Button = findViewById(R.id.pastDay)

        val calendar: Calendar = Calendar.getInstance()
        var year = calendar.get(Calendar.YEAR)
        var day = calendar.get(Calendar.DAY_OF_MONTH)
        var month = calendar.get(Calendar.MONTH)
        val parse = SchedueParsing(dateInput, calendar, itemList, this)

        updateDate(dateInput, day, month, year)

        itemList.layoutManager = LinearLayoutManager(this)


        checkSchedue.setOnClickListener {
            parse.setSchedueToday(getGroupID(groupInput))
        }

        nextSchedueDay.setOnClickListener {
            day++
            updateDate(dateInput, day, month, year)
            parse.setSchedueToday(getGroupID(groupInput))
        }

        pastSchedueDay.setOnClickListener {
            day--
            updateDate(dateInput, day, month, year)
            parse.setSchedueToday(getGroupID(groupInput))
        }

    }

    fun getGroupID(groupInput: TextView): String {

        lateinit var ID: String
        try {
            val input = groupInput.text.toString().uppercase()
            Log.d("GroupInput", "${groupInput.text}")
            Log.d("Input", "$input")
            when (input) {
                "ГД-123К" -> ID = "187346"
                "ДО-123" -> ID = "187347"
                "ДО-222" -> ID = "187348"
                "ДО-321" -> ID = "187349"
                "ДО-321Б" -> ID = "187350"
                "ДО-420" -> ID = "187351"
                "ИКС-123" -> ID = "187352"
                "ИКС-123П" -> ID = "187353"
                "ИКС-222К" -> ID = "187354"
                "К-222" -> ID = "187355"
                "К-321" -> ID = "187356"
                "МСР-123К" -> ID = "187357"
                "П-123" -> ID = "187358"
                "П-222" -> ID = "187359"
                "П-321" -> ID = "187360"
                "ПК-420" -> ID = "187361"
                "ПКС-321" -> ID = "187362"
                "ПКС-420" -> ID = "187363"
                "ПНК-123К" -> ID = "187364"
                "ПНК-222К" -> ID = "187365"
                "РМС-123" -> ID = "187367"
                "РМС-222" -> ID = "187368"
                "РМС-321" -> ID = "187369"
                "СД-123К" -> ID = "187370"
                "СД-123ОЗ" -> ID = "187371"
                "СП-222К" -> ID = "187373"
                "СП-320" -> ID = "187374"
                "СП-420" -> ID = "187375"
                "ТА-123" -> ID = "187376"
                "ТА-222" -> ID = "187377"
                "ТА-321" -> ID = "187378"
                "ТА-420" -> ID = "187379"
                "ТО-123" -> ID = "187380"
                "ТО-123П" -> ID = "187381"
                "ТО-222" -> ID = "187382"
                "ТО-222К" -> ID = "187383"
                "ТО-321" -> ID = "187384"
                "ТО-420" -> ID = "187385"
                "ТП-420" -> ID = "187386"
                "ТЭ-123" -> ID = "187387"
                "ТЭ-123П" -> ID = "187388"
                "ТЭ-222" -> ID = "187389"
                "ТЭ-321" -> ID = "187390"
                "ТЭ-420" -> ID = "187391"
                "ТЭ-414Б" -> ID = "187392"
                "Э-123" -> ID = "187393"
                "Э-320" -> ID = "187394"
                "ЭМ-222К" -> ID = "187395"
                "ЭМ-321" -> ID = "187396"
            }
            return ID
        } catch (e: Exception) {
            Toast.makeText(this, "Ошибка ввода", Toast.LENGTH_SHORT).show()
        }
        return "0"
    }

    fun updateDate(dateInput: TextView, day: Int, month: Int, year: Int) {
        if (month < 10) dateInput.text = "$day.0${month + 1}.$year"
        else dateInput.text = "$day.${month + 1}.$year"
    }
}
