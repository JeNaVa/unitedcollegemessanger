package com.example.upc_schedue

class Item(
    val lessonId: String = "",
    val name: String,
    val time: String,
    val lessonRoom: String,
) {
}